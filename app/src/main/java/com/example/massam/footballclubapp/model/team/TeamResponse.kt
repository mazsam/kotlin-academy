package com.example.massam.footballclubapp.model.team

data class TeamResponse (
    val teams: List<Team>
)