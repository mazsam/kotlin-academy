package com.example.massam.footballclubapp.presenter

import android.util.Log
import com.example.massam.footballclubapp.api.ApiRepository
import com.example.massam.footballclubapp.api.TheSportDBApi
import com.example.massam.footballclubapp.model.event.EventResponse
import com.example.massam.footballclubapp.model.league.LeagueResponse
import com.example.massam.footballclubapp.view.match.MatchView
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MatchPresenter(val view: MatchView) {

    val apiRepository = ApiRepository()
    val gson = Gson()

    var match  = 1


    fun getAllLeague() {
        view.showLoading()

        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.leagueAll()),
                    LeagueResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showLeagueList(data)
            }
        }
    }

    fun getEventsPrev(id: String?) {
        match = 1
        view.showLoading()

        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getLeaguePrev(id!!)),
                    EventResponse::class.java
            )

            uiThread {
                view.hideLoading()

                try {
                    view.eventPrev(data.events!!)
                } catch (e: NullPointerException) {
                    view.showEmptyData()
                }
            }
        }
    }

    fun getEventsNext(id: String?) {
        match = 2
        view.showLoading()

        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getLeagueNext(id!!)),
                    EventResponse::class.java
            )

            uiThread {
                view.hideLoading()

                try {
                    view.eventNext(data.events!!)
                } catch (e: NullPointerException) {
                    view.showEmptyData()
                }
            }
        }
    }

}