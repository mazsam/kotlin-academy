package com.example.massam.footballclubapp.presenter

import com.example.massam.footballclubapp.api.ApiRepository
import com.example.massam.footballclubapp.api.TheSportDBApi
import com.example.massam.footballclubapp.model.team.TeamResponse
import com.example.massam.footballclubapp.view.detail.DetailView
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DetailPresenter(val view: DetailView) {

    val apiRepository = ApiRepository()
    val gson = Gson()

    fun getTeamDetails(idHomeTeam: String, idAwayTeam: String) {
        view.showLoading()

        doAsync {
            val dataHomeTeam = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeamDetails(idHomeTeam)),
                    TeamResponse::class.java
            )

            val dataAwayTeam = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeamDetails(idAwayTeam)),
                    TeamResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTeamDetails(dataHomeTeam.teams!!, dataAwayTeam.teams!!)
            }
        }
    }
}