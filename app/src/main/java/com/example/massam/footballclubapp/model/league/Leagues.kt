package com.example.massam.footballclubapp.model.league

data class Leagues(val idLeague: String?, val strLeague: String?) {
    override fun toString(): String {
        return strLeague.toString()
    }
}
