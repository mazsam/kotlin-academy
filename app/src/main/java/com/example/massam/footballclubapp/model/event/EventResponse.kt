package com.example.massam.footballclubapp.model.event

data class EventResponse(val events: List<Events>)