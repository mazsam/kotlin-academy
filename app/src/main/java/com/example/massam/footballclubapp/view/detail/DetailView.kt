package com.example.massam.footballclubapp.view.detail

import com.example.massam.footballclubapp.model.team.Team

interface DetailView {

    fun showLoading()
    fun hideLoading()
    fun showTeamDetails(dataHomeTeam: List<Team>, dataAwayTeam: List<Team>)
}
