package com.example.massam.footballclubapp.view.match

import com.example.massam.footballclubapp.model.event.Events
import com.example.massam.footballclubapp.model.league.LeagueResponse

interface MatchView {
    fun showLoading()
    fun hideLoading()
    fun showEmptyData()
    fun showLeagueList(data: LeagueResponse)
    fun eventNext(data: List<Events>)
    fun eventPrev(data: List<Events>)
}