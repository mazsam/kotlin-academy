package com.example.massam.footballclubapp.adapter

import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.massam.footballclubapp.R
import com.example.massam.footballclubapp.model.event.Events
import com.example.massam.footballclubapp.utils.DateTime
import org.jetbrains.anko.*

class MatchAdapter(val items: List<Events>, val clickListener: (Events) -> Unit) : RecyclerView.Adapter<MatchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(ItemUI().createView(AnkoContext.create(parent.context, parent)))

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        holder.bind(items[position], clickListener)
    }

    override fun getItemCount() = items.size

    companion object {
        val ID_DATE = 1
        val ID_HOME_TEAM = 2
        val ID_HOME_SCORE = 3
        val ID_AWAY_TEAM = 4
        val ID_AWAY_SCORE = 5
    }

    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view){

        val date: TextView = view.findViewById(ID_DATE)
        val home_team: TextView = view.findViewById(ID_HOME_TEAM)
        val home_score: TextView = view.findViewById(ID_HOME_SCORE)
        val away_team: TextView = view.findViewById(ID_AWAY_TEAM)
        val away_score: TextView = view.findViewById(ID_AWAY_SCORE)

        fun bind(item: Events, clickListener: (Events) -> Unit) {
            date.text = DateTime.getLongDate(item.dateEvent!!)
            home_team.text = item.strHomeTeam
            home_score.text = item.intHomeScore
            away_team.text = item.strAwayTeam
            away_score.text = item.intAwayScore

            itemView.setOnClickListener { clickListener(item) }
        }
    }

    inner class ItemUI : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
            linearLayout {
                lparams(matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL

                linearLayout {
                    backgroundColor = Color.parseColor("#ecf0f1")
                    orientation = LinearLayout.VERTICAL
                    padding = dip(8)

                    textView {
                        id = ID_DATE
                        textColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                        gravity = Gravity.CENTER
                    }.lparams(matchParent, wrapContent)

                    linearLayout {
                        gravity = Gravity.CENTER_VERTICAL

                        textView {
                            id = ID_HOME_TEAM
                            gravity = Gravity.CENTER
                            textSize = 18f
                            text = "home"
                        }.lparams(matchParent, wrapContent, 1f)

                        linearLayout {
                            gravity = Gravity.CENTER_VERTICAL

                            textView {
                                id = ID_HOME_SCORE
                                padding = dip(8)
                                textSize = 20f
                                setTypeface(null, Typeface.BOLD)
                                text = "0"
                            }

                            textView {
                                text = "vs"
                            }

                            textView {
                                id = ID_AWAY_SCORE
                                padding = dip(8)
                                textSize = 20f
                                setTypeface(null, Typeface.BOLD)
                                text = "0"
                            }
                        }

                        textView {
                            id = ID_AWAY_TEAM
                            gravity = Gravity.CENTER
                            textSize = 18f
                            text = "away"
                        }.lparams(matchParent, wrapContent, 1f)
                    }
                }.lparams(matchParent, matchParent) {
                    setMargins(dip(16), dip(8), dip(16), dip(8))
                }
            }
        }
    }
}
