package com.example.massam.footballclubapp.view

import com.example.massam.footballclubapp.model.team.Team

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
}