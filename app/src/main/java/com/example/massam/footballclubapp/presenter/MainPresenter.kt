package com.example.massam.footballclubapp.presenter

import com.example.massam.footballclubapp.api.ApiRepository
import com.example.massam.footballclubapp.view.MainView
import com.example.massam.footballclubapp.api.TheSportDBApi
import com.example.massam.footballclubapp.model.team.TeamResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainPresenter(private val view: MainView, private val apiRepository: ApiRepository, private val gson: Gson ) {
    fun getTeamList(league: String? ){
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeams(league)),
                    TeamResponse::class.java
            )
            uiThread {
                view.hideLoading()
                view.showTeamList(data.teams)
            }
        }
    }
}